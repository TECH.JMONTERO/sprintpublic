#!/bin/bash
#==========================================
#GENERADOR DE PROYECTOS GITLAB
#Prodigio.tech
#Autor: Samuel Garcia
#==========================================
#VARIABLES
#TOKENGITLAB="";
#NAMESPACEID=58050510;
#IDROOTPROJECT=39801779;

RULEXMLPATH="rule.xml";
RULEXSDPATH="rule.xsd";

ERROR="";
QUEUEREPOEXISTENTE="";
QUEUEREPOERROR="";
QUEUEREPOCREADOS="";

CONTADOR_REPOS_EXISTENTES=0;
CONTADOR_REPOS_ERROR=0;
CONTADOR_REPOS_CREADOS=0;

echo INFO VARS INPUT
#echo NAMESPACEID=$NAMESPACEID;
echo RULEXMLPATH=$RULEXMLPATH;
echo RULEXSDPATH=$RULEXSDPATH;
echo IDROOTPROJECT=$IDROOTPROJECT;

echo GROUPNAME=$GROUPNAME
echo PROJECTNAME=$PROJECTNAME
echo PROJECTNAME_DESC=$PROJECTNAME_DESC
echo IMPORTURL=$IMPORTURL
echo ISSUEID=$ISSUEID
echo REQNAME=$REQNAME


#VALIDAR EXISTENCIA DE PARSER XML
if [ $(ls -l $RULEXMLPATH | wc -l) -lt 1 ]
then
ERROR="Error  (003) - $RULEXMLPATH fichero no encontrado"; echo $ERROR; exit 1;
fi

if [ $(ls -l $RULEXSDPATH | wc -l) -lt 1 ]
then
ERROR="Error  (004) - $RULEXSDPATH fichero no encontrado"; echo $ERROR; exit 1;
fi


 
#VERIFICAR LA EXISTENCIA DE CADA REPO; Y SI NO EXISTE CREAR

#OBTENER EL ID DEL NAMESPACE
NAMESPACEID=$( curl --header "PRIVATE-TOKEN: ${TOKENGITLAB}" "https://gitlab.com/api/v4/namespaces?search=$GROUPNAME" | awk -F ":" '/id/{print $2}' | awk -F "," '{print $1}');

if [ -z "$NAMESPACEID" ]; then
 echo "Error  (005) No existe el grupo: $GROUPNAME"; exit 1;
 else

RESPONSE=$(curl --header "PRIVATE-TOKEN: ${TOKENGITLAB}" "https://gitlab.com/api/v4/groups/${NAMESPACEID}/projects?search=${PROJECTNAME}");
echo RESPONSE=$RESPONSE

EXISTE=$(echo $RESPONSE | wc -c);
echo EXISTE=$EXISTE

if [ $EXISTE -lt 4 ]
then
#CREAR EL REPO
#EXTRAER TODOS LOS VALORES REQUERIDOS
#ESTO AHORA VIENE DESDE LAS VARS PIPELINE

#VERIFICACION DE NOMENCLATURA PERMITIDA
REGX=$(xmllint --xpath "/rule/regular-expression/text()" $RULEXMLPATH);
CUMPLE=$([[ $PROJECTNAME =~ $REGX ]] && echo "yes" || echo "no")

if [ "$CUMPLE" = "yes" ]
then
echo "SI CUMPLE CON LA TAXONOMIA"

#VERIFICACION DE TIPO DE CREACION (1=CON MOLDE, 2=CON TEMPLATE GITLAB, 3=SIMPLE)
if [[ $TIPO -lt 1 || $TIPO -gt 3 ]]
then
echo "$PROJECTNAME"="TIPO DE REPO NO SORPORTADO";
QUEUEREPOERROR=$QUEUEREPOERROR""$PROJECTNAME"=TIPO DE REPO NO SORPORTADO|";
else
echo "TIPO DE REPO VALIDADO OK";

#INSERCION REPO CON PLANTILLA CUSTOM
if [ $TIPO -eq 1 ]
then
echo "TIPO DE INSERCION=1";
echo NAMESPACEID=$NAMESPACEID

PREFIJO="https://username:"$TOKENGITLAB"@gitlab.com";
PRIVATEIMPORTURL=$PREFIJO$IMPORTURL;

curl --request POST --header "PRIVATE-TOKEN: $TOKENGITLAB" --header "Content-Type: application/json" --data "{\"name\": \"$PROJECTNAME\", \"description\": \"$PROJECTNAME\", \"path\": \"$PROJECTNAME\", \"namespace_id\": \"$NAMESPACEID\", \"initialize_with_readme\": \"false\", \"import_url\": \"$PRIVATEIMPORTURL\"}" --url 'https://gitlab.com/api/v4/projects/' -D reponseheaders.txt > reponsebody.txt;


http_code=$(cat reponseheaders.txt | awk '/HTTP/ {print $2}');

if [ $http_code -eq 201 ]
then
CONTADOR_REPOS_CREADOS=$(($CONTADOR_REPOS_CREADOS + 1));

QUEUEREPOCREADOS="${QUEUEREPOCREADOS} TIPO=${TIPO}, REPO=${PROJECTNAME}:OK|";


##ENVIANDO LAS NOTIFICACIONES AL ISSUE (ISSUE TRACKER GITLAB)
MESSAGE="$REQNAME - The Project requested as ${PROJECTNAME} was created OK";
#curl --request POST --header "PRIVATE-TOKEN: $TOKENGITLAB" "https://gitlab.com/api/v4/projects/$IDROOTPROJECT/issues/$ISSUEID/notes?body=$MESSAGE" -D reponseheadersnota.txt > reponsebodynota.txt;

## ENVIANDO LAS NOTIFICACIONES AL ISSUE (ISSUE TRACKER JIRA CLOUD)
curl --request POST \
  --url "https://banchile.atlassian.net/rest/api/3/issue/$ISSUEID/comment" \
  --user "${ACCOUNTJIRACLOUD}:${TOKENJIRACLOUD}" \
  --header 'Accept: application/json' \
  --header 'Content-Type: application/json' \
  --data "{
  \"body\": {
    \"type\": \"doc\",
    \"version\": 1,
    \"content\": [
      {
        \"type\": \"paragraph\",
        \"content\": [
          {
            \"text\": \"$MESSAGE\",
            \"type\": \"text\"
          }
        ]
      }
    ]
  }
}" -D reponseheadersnota.txt > reponsebodynota.txt;

##CERRANDO EL ISSUE (ISSUE TRACKER GITLAB)
#curl --request PUT --header "PRIVATE-TOKEN: $TOKENGITLAB" "https://gitlab.com/api/v4/projects/$IDROOTPROJECT/issues/$ISSUEID?state_event=close" -D reponseheadersclose.txt > reponsebodyclose.txt;


##CERRANDO EL ISSUE (ISSUE TRACKER JIRA CLOUD)
curl --request POST \
  --url "https://banchile.atlassian.net/rest/api/3/issue/$ISSUEID/transitions" \
  --user "${ACCOUNTJIRACLOUD}:${TOKENJIRACLOUD}" \
  --header 'Accept: application/json' \
  --header 'Content-Type: application/json' \
  --data '{
  "transition": {
    "id": "31",
    "looped": true
  },
    "extraData": {
      "Iteration": "10a",
      "Step": "4"
    },
    "description": "From the order testing process",
    "generator": {
      "id": "mysystem-1",
      "type": "mysystem-application"
    },
    "cause": {
      "id": "myevent",
      "type": "mysystem-event"
    },
    "activityDescription": "Complete order processing",
    "type": "myplugin:type"
  }

  }
}'  -D reponseheadersclose.txt > reponsebodyclose.txt;



else
CONTADOR_REPOS_ERROR=$(($CONTADOR_REPOS_ERROR + 1));
ERRORDETAIL=$(cat reponsebody.txt | sed 's/"//g');
export QUEUEREPOERROR="${QUEUEREPOERROR} TIPO=${TIPO}, REPO=${PROJECTNAME}: ERROR FROM GITLAB [HTTPCODE=${http_code}, MSG=${ERRORDETAIL}]|";


fi

fi

#INSERCION REPO CON PLANTILLA GITLAB
if [ $TIPO -eq 2 ]
then

echo "TIPO DE INSERCION=2";
curl  --request POST --header "PRIVATE-TOKEN: $TOKENGITLAB" --header "Content-Type: application/json" --data "{\"name\": \"$PROJECTNAME\", \"description\": \"$PROJECTNAME\", \"path\": \"$PROJECTNAME\",\"namespace_id\": \"$NAMESPACEID\", \"initialize_with_readme\": \"false\", \"template_name\": \"$TEMPLATE\"}" --url 'https://gitlab.com/api/v4/projects/' -D reponseheaders.txt > reponsebody.txt;


http_code=$(cat reponseheaders.txt | awk '/HTTP/ {print $2}');

if [ $http_code -eq 201 ]
then
CONTADOR_REPOS_CREADOS=$(($CONTADOR_REPOS_CREADOS + 1));
QUEUEREPOCREADOS="${QUEUEREPOCREADOS} TIPO=${TIPO}, REPO=${PROJECTNAME}:OK|";

##ENVIANDO LAS NOTIFICACIONES AL ISSUE (ISSUE TRACKER GITLAB)
MESSAGE="$REQNAME - The Project requested as ${PROJECTNAME} was created OK";
#curl --request POST --header "PRIVATE-TOKEN: $TOKENGITLAB" "https://gitlab.com/api/v4/projects/$IDROOTPROJECT/issues/$ISSUEID/notes?body=$MESSAGE" -D reponseheadersnota.txt > reponsebodynota.txt;

## ENVIANDO LAS NOTIFICACIONES AL ISSUE (ISSUE TRACKER JIRA CLOUD)
curl --request POST \
  --url "https://banchile.atlassian.net/rest/api/3/issue/$ISSUEID/comment" \
  --user "${ACCOUNTJIRACLOUD}:${TOKENJIRACLOUD}" \
  --header 'Accept: application/json' \
  --header 'Content-Type: application/json' \
  --data "{
  \"body\": {
    \"type\": \"doc\",
    \"version\": 1,
    \"content\": [
      {
        \"type\": \"paragraph\",
        \"content\": [
          {
            \"text\": \"$MESSAGE\",
            \"type\": \"text\"
          }
        ]
      }
    ]
  }
}" -D reponseheadersnota.txt > reponsebodynota.txt;

##CERRANDO EL ISSUE (ISSUE TRACKER GITLAB)
#curl --request PUT --header "PRIVATE-TOKEN: $TOKENGITLAB" "https://gitlab.com/api/v4/projects/$IDROOTPROJECT/issues/$ISSUEID?state_event=close" -D reponseheadersclose.txt > reponsebodyclose.txt;

##CERRANDO EL ISSUE (ISSUE TRACKER JIRA CLOUD)
curl --request POST \
  --url "https://banchile.atlassian.net/rest/api/3/issue/$ISSUEID/transitions" \
  --user "${ACCOUNTJIRACLOUD}:${TOKENJIRACLOUD}" \
  --header 'Accept: application/json' \
  --header 'Content-Type: application/json' \
  --data '{
  "transition": {
    "id": "31",
    "looped": true
  },
    "extraData": {
      "Iteration": "10a",
      "Step": "4"
    },
    "description": "From the order testing process",
    "generator": {
      "id": "mysystem-1",
      "type": "mysystem-application"
    },
    "cause": {
      "id": "myevent",
      "type": "mysystem-event"
    },
    "activityDescription": "Complete order processing",
    "type": "myplugin:type"
  }

  }
}'  -D reponseheadersclose.txt > reponsebodyclose.txt;
else

CONTADOR_REPOS_ERROR=$(($CONTADOR_REPOS_ERROR + 1));
ERRORDETAIL=$(cat reponsebody.txt | sed 's/"//g');
export QUEUEREPOERROR="${QUEUEREPOERROR} TIPO=${TIPO}, REPO=${PROJECTNAME}: ERROR FROM GITLAB [HTTPCODE=${http_code}, MSG=${ERRORDETAIL}]|";

fi

fi


#INSERCION SIMPLE
if [ $TIPO -eq 3 ]
then
echo "TIPO DE INSERCION=3";
curl --write-out "%{http_code}\n" --request POST --header "PRIVATE-TOKEN: $TOKENGITLAB" --header "Content-Type: application/json" --data "{\"name\": \"$PROJECTNAME\", \"description\": \"$PROJECTNAME\", \"path\": \"$PROJECTNAME\",\"namespace_id\": \"$NAMESPACEID\", \"initialize_with_readme\": \"true\"}" --url 'https://gitlab.com/api/v4/projects/' -D reponseheaders.txt > reponsebody.txt;

http_code=$(cat reponseheaders.txt | awk '/HTTP/ {print $2}');
if [ $http_code -eq 201 ]
then
CONTADOR_REPOS_CREADOS=$(($CONTADOR_REPOS_CREADOS + 1));
QUEUEREPOCREADOS="${QUEUEREPOCREADOS} TIPO=${TIPO}, REPO=${PROJECTNAME}:OK|";

##ENVIANDO LAS NOTIFICACIONES AL ISSUE (ISSUE TRACKER GITLAB)
MESSAGE="$REQNAME - The Project requested as ${PROJECTNAME} was created OK";
#curl --request POST --header "PRIVATE-TOKEN: $TOKENGITLAB" "https://gitlab.com/api/v4/projects/$IDROOTPROJECT/issues/$ISSUEID/notes?body=$MESSAGE" -D reponseheadersnota.txt > reponsebodynota.txt;





## ENVIANDO LAS NOTIFICACIONES AL ISSUE (ISSUE TRACKER JIRA CLOUD)
curl --request POST \
  --url "https://banchile.atlassian.net/rest/api/3/issue/$ISSUEID/comment" \
  --user "${ACCOUNTJIRACLOUD}:${TOKENJIRACLOUD}" \
  --header 'Accept: application/json' \
  --header 'Content-Type: application/json' \
  --data "{
  \"body\": {
    \"type\": \"doc\",
    \"version\": 1,
    \"content\": [
      {
        \"type\": \"paragraph\",
        \"content\": [
          {
            \"text\": \"$MESSAGE\",
            \"type\": \"text\"
          }
        ]
      }
    ]
  }
}" -D reponseheadersnota.txt > reponsebodynota.txt;

##CERRANDO EL ISSUE (ISSUE TRACKER GITLAB)
#curl --request PUT --header "PRIVATE-TOKEN: $TOKENGITLAB" "https://gitlab.com/api/v4/projects/$IDROOTPROJECT/issues/$ISSUEID?state_event=close" -D reponseheadersclose.txt > reponsebodyclose.txt;

##CERRANDO EL ISSUE (ISSUE TRACKER JIRA CLOUD)
curl --request POST \
  --url "https://banchile.atlassian.net/rest/api/3/issue/$ISSUEID/transitions" \
  --user "${ACCOUNTJIRACLOUD}:${TOKENJIRACLOUD}" \
  --header 'Accept: application/json' \
  --header 'Content-Type: application/json' \
  --data '{
  "transition": {
    "id": "31",
    "looped": true
  },
    "extraData": {
      "Iteration": "10a",
      "Step": "4"
    },
    "description": "From the order testing process",
    "generator": {
      "id": "mysystem-1",
      "type": "mysystem-application"
    },
    "cause": {
      "id": "myevent",
      "type": "mysystem-event"
    },
    "activityDescription": "Complete order processing",
    "type": "myplugin:type"
  }

  }
}'  -D reponseheadersclose.txt > reponsebodyclose.txt;
else

CONTADOR_REPOS_ERROR=$(($CONTADOR_REPOS_ERROR + 1));
ERRORDETAIL=$(cat reponsebody.txt | sed 's/"//g');
export QUEUEREPOERROR="${QUEUEREPOERROR} TIPO=${TIPO}, REPO=${PROJECTNAME}: ERROR FROM GITLAB [HTTPCODE=${http_code}, MSG=${ERRORDETAIL}]|";

fi

fi

fi
else
echo "NO CUMPLE CON LA TAXONOMIA"
CONTADOR_REPOS_ERROR=$(($CONTADOR_REPOS_ERROR + 1));
export QUEUEREPOERROR="${QUEUEREPOERROR} REPO=${PROJECTNAME}:NO CUMPLE LA TAXONOMIA|";
fi


echo "";
echo "";
 

else
#INFO REPOSITORIO EXISTE
CONTADOR_REPOS_EXISTENTES=$(($CONTADOR_REPOS_EXISTENTES + 1));
QUEUEREPOEXISTENTE="${QUEUEREPOEXISTENTE} REPO=${PROJECTNAME}|";
fi
fi;
echo "-REPOSITORIOS PRE-EXISTENTES:" $CONTADOR_REPOS_EXISTENTES;
echo $QUEUEREPOEXISTENTE
echo
echo "-REPOSITORIOS CON ERRORES:" $CONTADOR_REPOS_ERROR;
echo $QUEUEREPOERROR
echo
echo "-REPOSITORIOS INSERTADOS OK:" $CONTADOR_REPOS_CREADOS;
echo $QUEUEREPOCREADOS

if [ -z "$QUEUEREPOERROR" ]; then
echo "";
else
 exit 1;
fi;
